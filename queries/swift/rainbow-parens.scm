(class_body
  "{" @opening
  "}" @closing ) @container

(enum_class_body
  "{" @opening
  "}" @closing ) @container

(protocol_body
  "{" @opening
  "}" @closing ) @container

(protocol_property_requirements
  "{" @opening
  "}" @closing ) @container

(attribute
  "(" @opening
  ")" @closing ) @container

(function_body
  "{" @opening
  "}" @closing ) @container

(function_declaration
  "(" @opening
  ")" @closing ) @container

(value_arguments
  "(" @opening
  ")" @closing ) @container

(tuple_type
  "(" @opening
  ")" @closing ) @container

(tuple_expression
  "(" @opening
  ")" @closing ) @container

(lambda_literal
  "{" @opening
  "}" @closing ) @container

(computed_property
  "{" @opening
  "}" @closing ) @container

(array_type
  "[" @opening
  "]" @closing ) @container

(array_literal
  "[" @opening
  "]" @closing ) @container

(switch_statement
  "{" @opening
  "}" @closing ) @container

(for_statement
  "{" @opening
  "}" @closing ) @container
